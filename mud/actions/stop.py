# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2
from mud.events import StopEvent

class StopAction(Action2):
    EVENT = StopEvent
    ACTION = "stop"
    RESOLVE_OBJECT = "resolve_for_operate"
