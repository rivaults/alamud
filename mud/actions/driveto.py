# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action3
from mud.events import DriveToEvent
from .action                      import Action2
from mud.events                   import TeleportEvent
from mud.models.mixins.containing import Containing
from mud.models                   import Player
import mud.game

class DriveToAction(Action3):
	EVENT = DriveToEvent
	ACTION = "drive-to"
	RESOLVE_OBJECT = "resolve_for_operate"
    
	def resolve_object2(self):
		world = mud.game.GAME.world
		loc = world.get(self.object2)
		if loc:
			locs = [loc]
		else:
			locs = []
			for k,v in world.items():
				if isinstance(v, Containing) and \
				not isinstance(v, Player) and \
				k.find(self.object2) != -1:
					locs.append(v)
		return locs
