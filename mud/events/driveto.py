# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event3
from .event import Event2
from .info  import InfoEvent
import mud.game
class DriveToEvent(Event3):
	NAME = "drive-to"
	
	def perform(self):
		if not self.object.has_prop("drivable"):
			self.fail()
			return self.inform("drive-to.failed-not-drivable")
		n = len(self.object2)
		if n == 0:
			self.fail()
			self.inform("teleport.not-found")
		elif n > 1:
			self.fail()
			self.inform("drive-to.ambiguous-dest")
		else:
			if self.object.has_prop("turned-off"):
				self.fail()
				return self.inform("drive-to.failed-turn-off")
			else:
				self.actor.move_to(self.object2[0])
				self.object.move_to(self.object2[0])
				self.inform("drive-to")
				InfoEvent(self.actor).execute()

	def get_event_templates(self):
		return self.actor.get_event_templates()
