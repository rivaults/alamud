# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2

class StopEvent(Event2):
	NAME = "stop"

	def perform(self):
		if not self.object.has_prop("startable"):
			self.fail()
			return self.inform("stop.failed-not-startable")
		elif self.object.has_prop("turned-off"):
			self.fail()
			return self.inform("stop.failed-already-stopped")
		self.inform("stop")
